using TwoHundred.Common.Enums;

namespace TwoHundred.Common.DTOs;

public record CompanyDetailsDto(Guid IdentityGuid, string CompanyName, CompanyType Type, string AddressLine1,
    string AddressLine2, string AddressZipCode, string AddressCity,
    HistoricalCompanyAddressDto[] HistoricalAddresses,
    ContractDto[] Contracts);