namespace TwoHundred.Common.DTOs;

public record ContractDto(Guid IdentityGuid, Guid SupplierId, Guid VendorId, DateTimeOffset SignedOn);