namespace TwoHundred.Common.DTOs;

public record HistoricalCompanyAddressDto(string Line1,
    string Line2, string ZipCode, string City, DateTimeOffset CreatedOn);