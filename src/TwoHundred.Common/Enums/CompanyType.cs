namespace TwoHundred.Common.Enums;

public enum CompanyType : byte
{
    Supplier,
    Vendor
}