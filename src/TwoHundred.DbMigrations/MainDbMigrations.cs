using System.Reflection;

namespace TwoHundred.DbMigrations;

public static class TwoHundredDbMigrations
{
    public static Assembly Assembly => Assembly.GetExecutingAssembly();
}