CREATE TABLE [dbo].[company]
(
    [Id] BIGINT IDENTITY(1,1) NOT NULL,
    [IdentityGuid] UNIQUEIDENTIFIER NOT NULL,
    [Name] NVARCHAR(50) NOT NULL,
    [Type] TINYINT NOT NULL,
    [AddressLine1] NVARCHAR(50) NOT NULL,
    [AddressLine2] NVARCHAR(50) NOT NULL,
    [AddressZipCode] NVARCHAR(10) NOT NULL,
    [AddressCity] NVARCHAR(20) NOT NULL,
    CONSTRAINT [PK_company] PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE UNIQUE NONCLUSTERED INDEX [IX_company_IdentityGuid]
    ON [dbo].[company]([IdentityGuid] ASC);