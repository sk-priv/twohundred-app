CREATE TABLE [dbo].[companyAddress]
(
    [Id] BIGINT IDENTITY(1,1) NOT NULL,
    [CompanyId] BIGINT NOT NULL,
    [Line1] NVARCHAR(50) NOT NULL,
    [Line2] NVARCHAR(50) NOT NULL,
    [ZipCode] NVARCHAR(10) NOT NULL,
    [City] NVARCHAR(20) NOT NULL,
    [CreatedDate] DATETIMEOFFSET NOT NULL,
    CONSTRAINT [PK_companyAddress] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO

ALTER TABLE [dbo].[companyAddress]  WITH CHECK ADD CONSTRAINT [FK_companyAddress_company] FOREIGN KEY([CompanyId])
    REFERENCES [dbo].[company] ([Id])
GO