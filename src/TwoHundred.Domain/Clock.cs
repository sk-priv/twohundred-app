using System.Runtime.CompilerServices;

[assembly:InternalsVisibleTo("TwoHundred.UnitTests")]

namespace TwoHundred.Domain;

public static class Clock
{
    private static readonly Func<DateTimeOffset> DefaultDateTimeProvider = () => DateTimeOffset.UtcNow;
    private static Func<DateTimeOffset> _dateTimeProvider = DefaultDateTimeProvider;

    internal static void SetCurrentDateTimeProvider(Func<DateTimeOffset> provider)
    {
        _dateTimeProvider = provider;
    }

    public static DateTimeOffset GetCurrentDateTime()
    {
        return _dateTimeProvider();
    }
}