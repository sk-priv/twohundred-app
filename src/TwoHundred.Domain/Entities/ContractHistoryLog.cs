using TwoHundred.Domain.BuildingBlocks;

namespace TwoHundred.Domain.Entities;

public class ContractHistoryLog : Entity
{
    public Guid ContractId { get; private set; }
    public Guid SupplierId { get; private set; }
    public Guid VendorId { get; private set; }
    public DateTimeOffset SignedOn { get; private set; }

    private ContractHistoryLog() {}
    
    public ContractHistoryLog(Guid contractId, Guid supplierId, Guid vendorId, DateTimeOffset signedOn)
    {
        ContractId = contractId;
        SupplierId = supplierId;
        VendorId = vendorId;
        SignedOn = signedOn;
    }
}