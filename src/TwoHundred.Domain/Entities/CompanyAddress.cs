using TwoHundred.Domain.BuildingBlocks;

namespace TwoHundred.Domain.Entities;

public class CompanyAddress : Entity
{
    public long CompanyId { get; private set; }
    public string Line1 { get; private set; }
    public string Line2 { get; private set; }
    public string ZipCode { get; private set; }
    public string City { get; private set; }
    public DateTimeOffset CreatedDate { get; private set; }

    private CompanyAddress() { }

    public CompanyAddress(string line1, string line2, string zipCode, string city, DateTimeOffset createdDate)
    {
        Line1 = line1;
        Line2 = line2;
        ZipCode = zipCode;
        City = city;
        CreatedDate = createdDate;
    }
}