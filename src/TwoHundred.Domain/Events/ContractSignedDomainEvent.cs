using TwoHundred.Domain.BuildingBlocks;

namespace TwoHundred.Domain.Events;

public record ContractSignedDomainEvent(Guid ContractId, Guid SupplierId, Guid VendorId, DateTimeOffset SignedOn) : IDomainEvent;