namespace TwoHundred.Domain.BuildingBlocks;

public abstract class IdentityEntity : Entity
{
    public Guid IdentityGuid { get; protected set; }

    protected IdentityEntity() : this(Guid.Empty)
    {
    }

    protected IdentityEntity(Guid identityGuid)
    {
        IdentityGuid = identityGuid != Guid.Empty
            ? identityGuid
            : Guid.NewGuid();
    }
}