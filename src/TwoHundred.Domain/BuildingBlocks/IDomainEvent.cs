using MediatR;

namespace TwoHundred.Domain.BuildingBlocks;

public interface IDomainEvent : INotification
{
}