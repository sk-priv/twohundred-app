namespace TwoHundred.Domain.BuildingBlocks;

public abstract class Entity
{
    public long Id { get; protected set; }
}