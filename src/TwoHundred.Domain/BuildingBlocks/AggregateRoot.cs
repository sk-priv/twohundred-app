namespace TwoHundred.Domain.BuildingBlocks;

public abstract class AggregateRoot : IdentityEntity
{
    private readonly List<IDomainEvent> _domainEvents = new();

    public virtual IReadOnlyCollection<IDomainEvent> DomainEvents => _domainEvents;

    protected AggregateRoot() { }

    protected AggregateRoot(Guid identityGuid) : base(identityGuid) { }

    protected void AddDomainEvent(IDomainEvent newEvent) => _domainEvents.Add(newEvent);

    public void ClearDomainEvents() => _domainEvents.Clear();
}