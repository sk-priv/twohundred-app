using TwoHundred.Common.Enums;
using TwoHundred.Domain.BuildingBlocks;
using TwoHundred.Domain.Entities;

namespace TwoHundred.Domain.Aggregates;

public class Company : AggregateRoot
{
    public string Name { get; private set; }
    public CompanyType Type { get; private set; }
    public string AddressLine1 { get; private set; }
    public string AddressLine2 { get; private set; }
    public string AddressZipCode { get; private set; }
    public string AddressCity { get; private set; }

    public IList<CompanyAddress> HistoricalAddresses { get; private set; } = new List<CompanyAddress>(); 

    private Company() { }

    public Company(Guid identityGuid, string name, CompanyType type, string addressLine1,
        string addressLine2, string addressZipCode, string addressCity)
    {
        IdentityGuid = identityGuid;
        Name = name;
        Type = type;
        AddressLine1 = addressLine1;
        AddressLine2 = addressLine2;
        AddressZipCode = addressZipCode;
        AddressCity = addressCity;
    }

    public void Update(string name, string addressLine1, string addressLine2,
        string addressZipCode, string addressCity)
    {
        Name = name;

        if (HasAddressChanged(addressLine1, addressLine2,
                addressZipCode, addressCity))
        {
            HistoricalAddresses.Add(
                new CompanyAddress(AddressLine1, AddressLine2, AddressZipCode, AddressCity,
                    Clock.GetCurrentDateTime()));
            
            AddressLine1 = addressLine1;
            AddressLine2 = addressLine2;
            AddressZipCode = addressZipCode;
            AddressCity = addressCity;
        }
    }

    private bool HasAddressChanged(string addressLine1, string addressLine2, string addressZipCode, string addressCity)
    {
        return AddressLine1 != addressLine1
               || AddressLine2 != addressLine2
               || AddressZipCode != addressZipCode
               || AddressCity != addressCity;

    }
}