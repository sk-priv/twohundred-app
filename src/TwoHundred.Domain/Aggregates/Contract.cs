using TwoHundred.Domain.BuildingBlocks;
using TwoHundred.Domain.Events;

namespace TwoHundred.Domain.Aggregates;

public class Contract : AggregateRoot
{
    public Guid SupplierId { get; private set; }
    public Guid VendorId { get; private set; }
    public DateTimeOffset SignedOn { get; private set; }

    private Contract()
    {
    }

    public static Contract Sign(Guid contractId, Guid supplierId, Guid vendorId, DateTimeOffset signedOn) =>
        new(contractId, supplierId, vendorId, signedOn);

    private Contract(Guid contractId, Guid supplierId, Guid vendorId, DateTimeOffset signedOn)
    {
        IdentityGuid = contractId;
        SupplierId = supplierId;
        VendorId = vendorId;
        SignedOn = signedOn;

        AddDomainEvent(new ContractSignedDomainEvent(contractId, supplierId, vendorId, signedOn));
    }
}