namespace TwoHundred.Domain.Exceptions;

public class ObjectNotFoundException : DomainException
{
    public ObjectNotFoundException(string message) : base(message)
    {
    }
}