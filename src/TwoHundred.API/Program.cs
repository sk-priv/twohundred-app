using DbUp;
using Microsoft.EntityFrameworkCore;
using TwoHundred.API.Infrastructure;
using TwoHundred.DbMigrations;
using TwoHundred.Infrastructure;
using TwoHundred.Infrastructure.Features.Companies;
using TwoHundred.Infrastructure.Features.Contracts;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(o =>
{
    o.UseInlineDefinitionsForEnums();
});

var connectionString = builder.Configuration.GetValue<string>("ConnectionString");

ConfigureDatabase();
ConfigureServices();

builder.Services.AddMediatR(config =>
{
    config.RegisterServicesFromAssemblies(TwoHundredInfrastructure.Assembly);
});

var app = builder.Build();

UpgradeDb();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();
app.UseExceptionHandler(e => e.UseCustomErrors());

app.Run();
return;


void UpgradeDb()
{
    var upgradeEngine = DeployChanges.To
        .SqlDatabase(connectionString)
        .WithScriptsEmbeddedInAssembly(TwoHundredDbMigrations.Assembly)
        .LogToConsole()
        .Build();

    var result = upgradeEngine.PerformUpgrade();
    if (result.Successful)
    {
        return;
    }
    
    Console.WriteLine(result.Error);
    app.WaitForShutdown();
}

void ConfigureDatabase()
{
    builder.Services
        .AddDbContext<TwoHundredContext>(options =>
            {
                options.UseSqlServer(connectionString, o =>
                    o.UseQuerySplittingBehavior(QuerySplittingBehavior.SingleQuery));
            }
        );
}

void ConfigureServices()
{
    builder.Services.AddScoped<ICompanyRepository, CompanyRepository>();
    builder.Services.AddScoped<IContractRepository, ContractRepository>();
    builder.Services.AddScoped<IContractHistoryLogRepository, ContractHistoryLogRepository>();

    builder.Services.AddScoped<ICompanyDetailsQuery, CompanyDetailsQuery>();
    builder.Services.AddScoped<ICompaniesQuery, CompaniesQuery>();
}
