using MediatR;
using Microsoft.AspNetCore.Mvc;
using TwoHundred.Common.DTOs;
using TwoHundred.Common.Enums;
using TwoHundred.Infrastructure.Features.Companies;

namespace TwoHundred.API.Features.Company;

[Route("api/v1/companies")]
public class CompaniesController : Controller
{
    private readonly IMediator _mediator;
    private readonly ICompanyDetailsQuery _companyDetailsQuery;
    private readonly ICompaniesQuery _companiesQuery;

    public CompaniesController(IMediator mediator,
        ICompanyDetailsQuery companyDetailsQuery,
        ICompaniesQuery companiesQuery)
    {
        _mediator = mediator;
        _companyDetailsQuery = companyDetailsQuery;
        _companiesQuery = companiesQuery;
    }
    
    [HttpGet, Route("{id:guid}")]
    public async Task<CompanyDetailsDto?> GetCompanyDetails(Guid id) =>
        await _companyDetailsQuery.Execute(id);

    [HttpGet]
    public async Task<CompanyDto[]> GetCompanies() =>
        await _companiesQuery.Execute();

    [HttpPost]
    public async Task AddCompany([FromBody] AddCompanyCommandDto dto) =>
        await _mediator.Send(new AddCompanyCommand(dto.CompanyId, dto.Name, dto.Type,
            dto.AddressLine1, dto.AddressLine2, dto.AddressZipCode, dto.AddressCity));

    [HttpPatch, Route("{id:guid}")]
    public async Task UpdateCompany([FromBody] UpdateCompanyCommandDto dto, Guid id) =>
        await _mediator.Send(new UpdateCompanyCommand(id, dto.Name, dto.AddressLine1,
            dto.AddressLine2, dto.AddressZipCode, dto.AddressCity));

    public record AddCompanyCommandDto(Guid CompanyId, string Name, CompanyType Type,
        string AddressLine1, string AddressLine2, string AddressZipCode, string AddressCity);
    
    public record UpdateCompanyCommandDto(string Name, string AddressLine1, string AddressLine2,
        string AddressZipCode, string AddressCity);
}