using MediatR;
using Microsoft.AspNetCore.Mvc;
using TwoHundred.Infrastructure.Features.Contracts;

namespace TwoHundred.API.Features.Contracts;

[Route("api/v1/contracts")]
public class ContractsController : Controller
{
    private readonly IMediator _mediator;

    public ContractsController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpPost, Route("sign")]
    public async Task SignContract([FromBody] SignContractCommandDto dto) =>
        await _mediator.Send(new SignContractCommand(dto.ContractId, dto.SupplierId, dto.VendorId, dto.SignedOn));
}

public record SignContractCommandDto(Guid ContractId, Guid SupplierId, Guid VendorId, DateTimeOffset SignedOn);