using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Text.Json;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using TwoHundred.Domain.Exceptions;

namespace TwoHundred.API.Infrastructure;

public static class CustomErrors
{
    public static void UseCustomErrors(this IApplicationBuilder app)
    {
        app.Use(WriteResponse);
    }

    private static async Task WriteResponse(HttpContext httpContext, Func<Task> next)
    {
        var exceptionDetails = httpContext.Features.Get<IExceptionHandlerFeature>();
        var ex = exceptionDetails?.Error;

        if (ex != null)
        {
            HttpStatusCode statusCode = ex switch
            {
                ObjectNotFoundException => HttpStatusCode.NotFound,
                ValidationException => HttpStatusCode.UnprocessableEntity,
                ArgumentNullException => HttpStatusCode.UnprocessableEntity,
                DomainException => HttpStatusCode.UnprocessableEntity,
                ArgumentException => HttpStatusCode.BadRequest,
                _ => HttpStatusCode.InternalServerError
            };

            var message = string.Empty;
            if (ex is DomainException domainException)
            {
                message = domainException.Message;
            }
            if (ex is ArgumentNullException argumentNullException)
            {
                message = "Missing value for parameter " + argumentNullException.ParamName;
            }

            httpContext.Response.ContentType = "application/problem+json";

            var problem = new ProblemDetails
            {
                Status = (int)statusCode,
                Title = $"An error occured. {message}",
                Detail = null
            };

            httpContext.Response.StatusCode = (int)statusCode;

            var stream = httpContext.Response.Body;
            await JsonSerializer.SerializeAsync(stream, problem);
        }
    }
}