using FluentAssertions;
using TwoHundred.Common.Enums;
using TwoHundred.Domain;
using TwoHundred.Domain.Aggregates;
using Xunit;

namespace TwoHundred.UnitTests;

public class CompanyTests
{
    private readonly DateTimeOffset _now = new DateTime(2023, 9, 12, 10, 0, 0, 0, DateTimeKind.Utc);

    public CompanyTests()
    {
        Clock.SetCurrentDateTimeProvider(() => _now);
    }

    [Fact]
    public void Given_NewCompanyData_When_CreatingACompany_Then_CompanyIsCreatedWithCorrectValues()
    {
        // Given
        const string name = "Test Corp";
        const CompanyType type = CompanyType.Vendor;
        const string addressLine1 = "Str 1";
        const string addressLine2 = "ext. line";
        const string zip = "80-400";
        const string city = "Gdansk";

        // When
        var company = new Company(Guid.NewGuid(), name, type, addressLine1, addressLine2, zip, city);

        // Then
        company.Name.Should().Be(name);
        company.Type.Should().Be(type);
        company.AddressLine1.Should().Be(addressLine1);
        company.AddressLine2.Should().Be(addressLine2);
        company.AddressZipCode.Should().Be(zip);
        company.AddressCity.Should().Be(city);

        company.HistoricalAddresses.Should().BeEmpty();
    }

    [Fact]
    public void Given_NewAddressData_When_UpdatingCompanyAddress_Then_HistoricalAddressesUpdatesCorrectly()
    {
        // Given
        const string name = "Test Corp";
        const CompanyType type = CompanyType.Vendor;
        const string addressLine1 = "Str 1";
        const string addressLine2 = "ext. line";
        const string zip = "80-400";
        const string city = "Gdansk";

        var company = new Company(Guid.NewGuid(), name, type, addressLine1, addressLine2, zip, city);

        // When
        company.Update("Updated Corp", "updated line1", "updated line2", "80-400", "updated city");

        // Then
        company.AddressLine1.Should().Be("updated line1");

        company.HistoricalAddresses.Should().ContainSingle();
        var historicalAddress = company.HistoricalAddresses.Single();

        historicalAddress.Line1.Should().Be(addressLine1);
        historicalAddress.Line2.Should().Be(addressLine2);
        historicalAddress.ZipCode.Should().Be(zip);
        historicalAddress.City.Should().Be(city);
        historicalAddress.CreatedDate.Should().Be(_now);
    }

    [Fact]
    public void Given_SameAddressData_When_UpdatingCompanyAddress_Then_HistoricalAddressesDoesNotUpdate()
    {
        // Given
        var company = CreateCompany(Guid.NewGuid());

        // When
        company.Update("updated name", company.AddressLine1, company.AddressLine2, company.AddressZipCode,
            company.AddressCity);

        // Then
        company.HistoricalAddresses.Should().BeEmpty();
    }

    private static Company CreateCompany(Guid newGuid) =>
        new(newGuid, "Test Corp", CompanyType.Supplier, "Str 1",
            "ext. line", "80-400", "Gdansk");
}