using FluentAssertions;
using NSubstitute;
using TwoHundred.Common.Enums;
using TwoHundred.Domain.Aggregates;
using TwoHundred.Domain.Exceptions;
using TwoHundred.Infrastructure.Features.Companies;
using TwoHundred.Infrastructure.Features.Contracts;
using Xunit;

namespace TwoHundred.UnitTests;

public class SignContractCommandHandlerTests
{
    [Fact]
    public async Task Given_ValidSupplierAndVendor_When_SigningContract_Then_ContractIsSigned()
    {
        // Given
        var contractRepository = Substitute.For<IContractRepository>();
        var companyRepository = Substitute.For<ICompanyRepository>();

        var contractId = Guid.NewGuid();
        var supplierId = Guid.NewGuid();
        var vendorId = Guid.NewGuid();
        var signedOn = DateTimeOffset.UtcNow;

        companyRepository.Get(supplierId).Returns(CreateCompany(supplierId, CompanyType.Supplier));
        companyRepository.Get(vendorId).Returns(CreateCompany(vendorId, CompanyType.Vendor));

        var command = new SignContractCommand(contractId, supplierId, vendorId, signedOn);
        var handler = new SignContractCommandHandler(contractRepository, companyRepository);

        // When
        var act = async () => await handler.Handle(command, default);

        // Then
        await act.Should().NotThrowAsync();
        await contractRepository.Received(1).AddAsync(Arg.Any<Contract>());
        await contractRepository.UnitOfWork.Received(1).SaveEntitiesAsync();
    }

    [Fact]
    public async Task Given_InvalidSupplier_When_SigningContract_Then_ExceptionIsThrown()
    {
        // Given
        var contractRepository = Substitute.For<IContractRepository>();
        var companyRepository = Substitute.For<ICompanyRepository>();

        var invalidSupplierId = Guid.NewGuid();
        companyRepository.Get(invalidSupplierId).Returns(CreateCompany(invalidSupplierId, CompanyType.Vendor));

        var command = new SignContractCommand(Guid.NewGuid(), invalidSupplierId, Guid.NewGuid(), DateTimeOffset.UtcNow);
        var handler = new SignContractCommandHandler(contractRepository, companyRepository);

        // When
        var act = async () => await handler.Handle(command, default);

        // Then
        await act.Should().ThrowAsync<DomainException>()
            .WithMessage($"Company with id {invalidSupplierId} is not a {CompanyType.Supplier}, which is required to sign a contract");
    }
    
    [Fact]
    public async Task Given_InvalidVendor_When_SigningContract_Then_ExceptionIsThrown()
    {
        // Given
        var contractRepository = Substitute.For<IContractRepository>();
        var companyRepository = Substitute.For<ICompanyRepository>();

        var supplierId = Guid.NewGuid();
        var invalidVendorId = Guid.NewGuid();
        companyRepository.Get(supplierId).Returns(CreateCompany(supplierId, CompanyType.Supplier));
        companyRepository.Get(invalidVendorId).Returns(CreateCompany(invalidVendorId, CompanyType.Supplier));

        var command = new SignContractCommand(Guid.NewGuid(), supplierId, invalidVendorId, DateTimeOffset.UtcNow);
        var handler = new SignContractCommandHandler(contractRepository, companyRepository);

        // When
        var act = async () => await handler.Handle(command, default);

        // Then
        await act.Should().ThrowAsync<DomainException>()
            .WithMessage($"Company with id {invalidVendorId} is not a {CompanyType.Vendor}, which is required to sign a contract");
    }

    private static Company CreateCompany(Guid invalidSupplierId, CompanyType companyType) =>
        new(invalidSupplierId, "Company name", companyType,
            "line1", "line2", "80-800", "City");
}