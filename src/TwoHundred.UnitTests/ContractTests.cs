using FluentAssertions;
using TwoHundred.Domain.Aggregates;
using TwoHundred.Domain.Events;
using Xunit;

namespace TwoHundred.UnitTests;

public class ContractTests
{
    [Fact]
    public void When_SigningContract_Then_ContractIsCorrectlySigned()
    {
        // Arrange
        var contractId = Guid.NewGuid();
        var supplierId = Guid.NewGuid();
        var vendorId = Guid.NewGuid();
        var signedOn = DateTimeOffset.UtcNow;

        // Act
        var contract = Contract.Sign(contractId, supplierId, vendorId, signedOn);

        // Assert
        contract.IdentityGuid.Should().Be(contractId);
        contract.SupplierId.Should().Be(supplierId);
        contract.VendorId.Should().Be(vendorId);
        contract.SignedOn.Should().Be(signedOn);

        contract.DomainEvents.Should().ContainSingle()
            .Which.Should().BeOfType<ContractSignedDomainEvent>();
    }
}