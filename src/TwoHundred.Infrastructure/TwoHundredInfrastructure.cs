using System.Reflection;

namespace TwoHundred.Infrastructure;

public static class TwoHundredInfrastructure
{
    public static Assembly Assembly => Assembly.GetExecutingAssembly();
}