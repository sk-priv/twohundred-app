using MediatR;
using Microsoft.EntityFrameworkCore;
using TwoHundred.Domain.BuildingBlocks;
using TwoHundred.Infrastructure.Interfaces;

namespace TwoHundred.Infrastructure.Base;

public class TransactionalContextBase : DbContext, IUnitOfWork
{
    private readonly IMediator _mediator;

    public TransactionalContextBase(DbContextOptions options,
        IMediator mediator) : base(options)
    {
        _mediator = mediator;
    }

    public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
    {
        var domainEvents = GetDomainEvents();
        await DispatchDomainEventsAsync(domainEvents);

        await SaveChangesAsync(cancellationToken);
        return true;
    }

    private IDomainEvent[] GetDomainEvents()
    {
        var domainEntities = ChangeTracker
            .Entries<AggregateRoot>()
            .Where(x => x.Entity.DomainEvents.Any())
            .ToList();
        var domainEvents = domainEntities
            .SelectMany(x => x.Entity.DomainEvents)
            .Distinct()
            .ToArray();

        domainEntities.ForEach(entity => entity.Entity.ClearDomainEvents());
        
        return domainEvents;
    }
    
    private async Task DispatchDomainEventsAsync(IDomainEvent[] domainEvents)
    {
        foreach (var evt in domainEvents)
        {
            await _mediator.Publish(evt);
        }
    }

}