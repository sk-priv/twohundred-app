using MediatR;

namespace TwoHundred.Infrastructure.Features.Companies;

public record UpdateCompanyCommand(Guid CompanyId, string Name, string AddressLine1, string AddressLine2, string AddressZipCode, string AddressCity) : IRequest;

public class UpdateCompanyCommandHandler : IRequestHandler<UpdateCompanyCommand>
{
    private readonly ICompanyRepository _companyRepository;

    public UpdateCompanyCommandHandler(ICompanyRepository companyRepository)
    {
        _companyRepository = companyRepository;
    }

    public async Task Handle(UpdateCompanyCommand request, CancellationToken cancellationToken)
    {
        var company = await _companyRepository.Get(request.CompanyId);
        
        company.Update(request.Name,
            request.AddressLine1,
            request.AddressLine2,
            request.AddressZipCode,
            request.AddressCity);

        await _companyRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
    }
}