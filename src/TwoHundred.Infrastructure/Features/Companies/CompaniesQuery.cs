using TwoHundred.Common.DTOs;

namespace TwoHundred.Infrastructure.Features.Companies;

public interface ICompaniesQuery
{
    Task<CompanyDto[]> Execute();
}

public class CompaniesQuery : ICompaniesQuery
{
    private readonly ICompanyRepository _companyRepository;

    public CompaniesQuery(ICompanyRepository companyRepository)
    {
        _companyRepository = companyRepository;
    }

    public async Task<CompanyDto[]> Execute() =>
        (await _companyRepository.GetAll()).Select(c => new CompanyDto(
            c.IdentityGuid, c.Name, c.Type,
            c.AddressLine1, c.AddressLine2, c.AddressZipCode, c.AddressCity)).ToArray();
}