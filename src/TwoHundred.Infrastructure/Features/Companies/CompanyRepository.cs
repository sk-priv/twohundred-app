using Microsoft.EntityFrameworkCore;
using TwoHundred.Domain.Aggregates;
using TwoHundred.Domain.Exceptions;
using TwoHundred.Infrastructure.Interfaces;

namespace TwoHundred.Infrastructure.Features.Companies;

public interface ICompanyRepository : IRepository<Company>
{
    void Add(Company company);
    void Update(Company company);
    Task<Company> Get(Guid id);
    Task<Company[]> GetAll();
}

public class CompanyRepository : ICompanyRepository
{
    private readonly TwoHundredContext _context;

    public IUnitOfWork UnitOfWork => _context;
    
    public CompanyRepository(TwoHundredContext context)
    {
        _context = context;
    }

    public void Add(Company company) => _context.Companies.Add(company);

    public void Update(Company company)
    {
        _context.Entry(company).State = EntityState.Modified;
        _context.Entry(company).Property(p => p.Id).IsModified = false;
    }

    public async Task<Company> Get(Guid id) =>
        await _context.Companies.Include(e => e.HistoricalAddresses).SingleOrDefaultAsync(c => c.IdentityGuid == id)
        ?? throw new ObjectNotFoundException($"Company with id {id} not found");

    public async Task<Company[]> GetAll() => await _context.Companies.Include(e => e.HistoricalAddresses).ToArrayAsync();

}