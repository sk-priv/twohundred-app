using TwoHundred.Common.DTOs;
using TwoHundred.Domain.Aggregates;
using TwoHundred.Domain.Entities;
using TwoHundred.Infrastructure.Features.Contracts;

namespace TwoHundred.Infrastructure.Features.Companies;

public interface ICompanyDetailsQuery
{
    Task<CompanyDetailsDto> Execute(Guid id);
}

public class CompanyDetailsQuery : ICompanyDetailsQuery
{
    private readonly ICompanyRepository _companyRepository;
    private readonly IContractRepository _contractRepository;

    public CompanyDetailsQuery(ICompanyRepository companyRepository, IContractRepository contractRepository)
    {
        _companyRepository = companyRepository;
        _contractRepository = contractRepository;
    }

    public async Task<CompanyDetailsDto> Execute(Guid id)
    {
        var company = await _companyRepository.Get(id);
        var contracts = await _contractRepository.GetByCompanyAsync(id);

        return new CompanyDetailsDto(company.IdentityGuid, company.Name, company.Type,
            company.AddressLine1, company.AddressLine2, company.AddressZipCode, company.AddressCity,
            company.HistoricalAddresses.Select(MapAddress).ToArray(), contracts.Select(MapContract).ToArray());
    }

    private static HistoricalCompanyAddressDto MapAddress(CompanyAddress a) =>
        new(a.Line1, a.Line2, a.ZipCode, a.City, a.CreatedDate);
    
    private static ContractDto MapContract(Contract a) =>
        new(a.IdentityGuid, a.SupplierId, a.VendorId, a.SignedOn);
}