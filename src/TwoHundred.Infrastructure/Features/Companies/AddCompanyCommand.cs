using MediatR;
using TwoHundred.Common.Enums;
using TwoHundred.Domain.Aggregates;

namespace TwoHundred.Infrastructure.Features.Companies;

public record AddCompanyCommand(Guid CompanyId, string Name, CompanyType Type, string AddressLine1, string AddressLine2, string AddressZipCode, string AddressCity) : IRequest;

public class AddCompanyCommandHandler : IRequestHandler<AddCompanyCommand>
{
    private readonly ICompanyRepository _companyRepository;

    public AddCompanyCommandHandler(ICompanyRepository companyRepository)
    {
        _companyRepository = companyRepository;
    }

    public async Task Handle(AddCompanyCommand request, CancellationToken cancellationToken)
    {
        _companyRepository.Add(
            new Company(request.CompanyId, request.Name, request.Type,
                request.AddressLine1, request.AddressLine2, request.AddressZipCode, request.AddressCity));

        await _companyRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
    }
}