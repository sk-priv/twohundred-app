using MediatR;
using TwoHundred.Domain.Entities;
using TwoHundred.Domain.Events;

namespace TwoHundred.Infrastructure.Features.Contracts;

public class ContractSignedEventHandler : INotificationHandler<ContractSignedDomainEvent>
{
    private readonly IContractHistoryLogRepository _contractHistoryLogRepository;

    public ContractSignedEventHandler(IContractHistoryLogRepository contractHistoryLogRepository)
    {
        _contractHistoryLogRepository = contractHistoryLogRepository;
    }

    public async Task Handle(ContractSignedDomainEvent notification, CancellationToken cancellationToken)
    {
        await _contractHistoryLogRepository.AddAsync(
            new ContractHistoryLog(
                notification.ContractId,
                notification.SupplierId,
                notification.VendorId,
                notification.SignedOn));
    }
}