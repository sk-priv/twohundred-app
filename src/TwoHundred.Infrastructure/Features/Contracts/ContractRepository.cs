using Microsoft.EntityFrameworkCore;
using TwoHundred.Domain.Aggregates;
using TwoHundred.Infrastructure.Interfaces;

namespace TwoHundred.Infrastructure.Features.Contracts;

public interface IContractRepository : IRepository<Contract>
{
    Task AddAsync(Contract contract);
    Task<Contract[]> GetByCompanyAsync(Guid companyId);
}

public class ContractRepository : IContractRepository
{
    private readonly TwoHundredContext _context;
    public IUnitOfWork UnitOfWork => _context;

    public ContractRepository(TwoHundredContext context)
    {
        _context = context;
    }

    public async Task AddAsync(Contract contract) => await _context.Contracts.AddAsync(contract);

    public async Task<Contract[]> GetByCompanyAsync(Guid companyId) =>
        await _context.Contracts.Where(c => c.SupplierId == companyId || c.VendorId == companyId).ToArrayAsync();
}