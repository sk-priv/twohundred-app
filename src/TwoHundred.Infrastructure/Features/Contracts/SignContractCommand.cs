using MediatR;
using TwoHundred.Common.Enums;
using TwoHundred.Domain.Aggregates;
using TwoHundred.Domain.Exceptions;
using TwoHundred.Infrastructure.Features.Companies;

namespace TwoHundred.Infrastructure.Features.Contracts;

public record SignContractCommand(Guid ContractId, Guid SupplierId, Guid VendorId, DateTimeOffset SignedOn) : IRequest;

public class SignContractCommandHandler : IRequestHandler<SignContractCommand>
{
    private readonly IContractRepository _contractRepository;
    private readonly ICompanyRepository _companyRepository;

    public SignContractCommandHandler(IContractRepository contractRepository, ICompanyRepository companyRepository)
    {
        _contractRepository = contractRepository;
        _companyRepository = companyRepository;
    }

    public async Task Handle(SignContractCommand request, CancellationToken cancellationToken)
    {
        var supplier = await GetSupplier(request);
        var vendor = await GetVendor(request);

        await _contractRepository.AddAsync(
            Contract.Sign(request.ContractId, supplier.IdentityGuid, vendor.IdentityGuid, request.SignedOn));

        await _contractRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
    }

    private async Task<Company> GetSupplier(SignContractCommand request)
    {
        var supplier = await _companyRepository.Get(request.SupplierId);
        if (supplier.Type != CompanyType.Supplier)
        {
            throw new DomainException(
                $"Company with id {supplier.IdentityGuid} is not a {CompanyType.Supplier}, which is required to sign a contract");
        }

        return supplier;
    }

    private async Task<Company> GetVendor(SignContractCommand request)
    {
        var vendor = await _companyRepository.Get(request.VendorId);
        if (vendor.Type != CompanyType.Vendor)
        {
            throw new DomainException(
                $"Company with id {vendor.IdentityGuid} is not a {CompanyType.Vendor}, which is required to sign a contract");
        }

        return vendor;
    }
}