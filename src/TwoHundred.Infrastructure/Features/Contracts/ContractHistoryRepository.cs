using Microsoft.EntityFrameworkCore;
using TwoHundred.Domain.Entities;

namespace TwoHundred.Infrastructure.Features.Contracts;

public interface IContractHistoryLogRepository
{
    Task AddAsync(ContractHistoryLog log);
    Task<ContractHistoryLog[]> GetAllAsync();
}

public class ContractHistoryLogRepository : IContractHistoryLogRepository
{
    private readonly TwoHundredContext _context;

    public ContractHistoryLogRepository(TwoHundredContext context)
    {
        _context = context;
    }

    public async Task AddAsync(ContractHistoryLog log) => await _context.ContractHistoryLogs.AddAsync(log);

    public async Task<ContractHistoryLog[]> GetAllAsync() => await _context.ContractHistoryLogs.ToArrayAsync();
}