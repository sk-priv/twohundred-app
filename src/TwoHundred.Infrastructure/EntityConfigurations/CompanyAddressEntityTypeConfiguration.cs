using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TwoHundred.Domain.Entities;

namespace TwoHundred.Infrastructure.EntityConfigurations;

public class CompanyAddressEntityTypeConfiguration : IEntityTypeConfiguration<CompanyAddress>
{
    public void Configure(EntityTypeBuilder<CompanyAddress> builder)
    {
        builder.ToTable("companyAddress");
        builder.HasKey(o => o.Id);
        builder.Property(o => o.Id).UseIdentityColumn();
    }
}