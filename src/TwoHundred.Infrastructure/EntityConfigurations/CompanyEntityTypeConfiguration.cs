using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TwoHundred.Domain.Aggregates;

namespace TwoHundred.Infrastructure.EntityConfigurations;

public class CompanyEntityTypeConfiguration : IEntityTypeConfiguration<Company>
{
    public void Configure(EntityTypeBuilder<Company> builder)
    {
        builder.ToTable("company");
        builder.HasKey(o => o.Id);
        builder.Property(o => o.Id).UseIdentityColumn();

        builder.HasMany(o => o.HistoricalAddresses)
            .WithOne()
            .HasForeignKey(o => o.CompanyId);
    }
}