using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TwoHundred.Domain.Entities;
using TwoHundred.Infrastructure.Features.Contracts;

namespace TwoHundred.Infrastructure.EntityConfigurations;

public class ContractHistoryLogEntityTypeConfiguration : IEntityTypeConfiguration<ContractHistoryLog>
{
    public void Configure(EntityTypeBuilder<ContractHistoryLog> builder)
    {
        builder.ToTable("contractHistoryLog");
        builder.HasKey(o => o.Id);
        builder.Property(o => o.Id).UseIdentityColumn();
    }
}