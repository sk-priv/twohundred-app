using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TwoHundred.Domain.Aggregates;

namespace TwoHundred.Infrastructure.EntityConfigurations;

public class ContractEntityTypeConfiguration : IEntityTypeConfiguration<Contract>
{
    public void Configure(EntityTypeBuilder<Contract> builder)
    {
        builder.ToTable("contract");
        builder.HasKey(o => o.Id);
        builder.Property(o => o.Id).UseIdentityColumn();
    }
}