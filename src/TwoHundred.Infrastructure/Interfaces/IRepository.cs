using TwoHundred.Domain.BuildingBlocks;

namespace TwoHundred.Infrastructure.Interfaces;

public interface IRepository<T> where T : AggregateRoot
{
    IUnitOfWork UnitOfWork { get; }
}