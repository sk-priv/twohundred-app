namespace TwoHundred.Infrastructure.Interfaces;

public interface IUnitOfWork
{
    Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default);
}