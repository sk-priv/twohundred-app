using MediatR;
using Microsoft.EntityFrameworkCore;
using TwoHundred.Domain.Aggregates;
using TwoHundred.Domain.Entities;
using TwoHundred.Infrastructure.Base;
using TwoHundred.Infrastructure.EntityConfigurations;
using TwoHundred.Infrastructure.Features.Contracts;

namespace TwoHundred.Infrastructure;

public class TwoHundredContext : TransactionalContextBase
{
    public DbSet<Company> Companies { get; private set; }
    public DbSet<Contract> Contracts { get; private set;}
    public DbSet<ContractHistoryLog> ContractHistoryLogs { get; private set;}
    public DbSet<CompanyAddress> CompanyAddresses { get; private set; }

    public TwoHundredContext(DbContextOptions<TwoHundredContext> options, IMediator mediator) : base(options, mediator)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfiguration(new CompanyEntityTypeConfiguration());
        modelBuilder.ApplyConfiguration(new ContractEntityTypeConfiguration());
        modelBuilder.ApplyConfiguration(new ContractHistoryLogEntityTypeConfiguration());
        modelBuilder.ApplyConfiguration(new CompanyAddressEntityTypeConfiguration());
    }
}