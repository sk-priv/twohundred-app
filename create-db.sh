#!/bin/bash

SERVER_IP="localhost"
SERVER_PORT="1433"
USERNAME="sa"
PASSWORD="QWeRrtYY123@"

QUERY="
IF NOT EXISTS (SELECT name FROM sys.databases WHERE name = 'TwoHundred')
BEGIN
    CREATE DATABASE [TwoHundred];
    PRINT 'Db ''TwoHundred'' is created successfully';
END
ELSE
    PRINT 'Db ''TwoHundred'' already exists. Skipping...';
GO

IF EXISTS (SELECT 1 FROM sys.database_principals WHERE name = 'TwoHundredUser')
    PRINT 'User TwoHundredUser already exists.'
ELSE
BEGIN
    PRINT 'User TwoHundredUser does not exist. Creating...';

    USE [TwoHundred];

    CREATE LOGIN TwoHundredUser WITH PASSWORD = 'Passwd123!';
    CREATE USER TwoHundredUser FOR LOGIN TwoHundredUser;
    EXEC sp_addrolemember 'db_owner', 'TwoHundredUser';

    PRINT 'User TwoHundredUser is created successfully';
END
"

echo "$QUERY" | sqlcmd -S "$SERVER_IP,$SERVER_PORT" -U $USERNAME -P $PASSWORD -N -C