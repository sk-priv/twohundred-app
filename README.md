# TwoHundred - Contracts

## Local development

### Prerequisites

Docker on WSL

### Before first run

1. For development purposes, use a Dockerized version of SQL Server:
    ```console
    docker run -e "ACCEPT_EULA=Y" -e "MSSQL_SA_PASSWORD=QWeRrtYY123@" -p 1433:1433 -d mcr.microsoft.com/mssql/server:2022-latest`
    ```

2. To create a database and a test user, execute `./create-db.sh` on WSL.

3. Check which IP to use to access WSL from a Windows machine (if applicable).
    ```console
    ip addr show eth0 | grep -oP '(?<=inet\s)\d+(\.\d+){3}'
    ```
    Get the IP address and modify `"ConnectionString"` in `src/TwoHundred.API/appsettings.Development.json`